#ifndef S3D_CAMERA_H
#define S3D_CAMERA_H

#include "input.h"
#include "transform.h"
#include "shader.h"
#include "window.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace s3d {
class Camera {
public:
    Camera();

    const s3d::Transform* transform() const;
    s3d::Transform* transform();
    const s3d::Input* input() const;
    s3d::Input* input();

    void setClearColor(const glm::vec3& color);
    const glm::vec3& getClearColor() const;

    void setFieldOfView(const float fieldOfView);
    const float getFieldOfView() const;

    void setAspectRatio(const float ratio);
    const float getAspectRatio() const;

    void setNearFar(const float nearPlane, const float farPlane);
    const float getNear() const;
    const float getFar() const;

    void look(s3d::Window* const window, const float dt);
    void move(s3d::Window* const window, const float dt);
    void update(s3d::Window* const window, const float dt);
    void attach(s3d::Shader* const shader) const;

    void setSensitivity(const float x, const float y);
    const float getSensitivityX() const;
    const float getSensitivityY() const;
private:
    const glm::mat4 proj() const;
    const glm::mat4 view() const;

    glm::vec3 mClearColor;

    float mFieldOfView;
    float mAspectRatio;
    float mNear;
    float mFar;

    s3d::Transform mTransform;
    s3d::Input mInput;

    float mSensX, mSensY;

public:
    static void setViewStr(const std::string& str = "s3d_camera_view");
    static void setProjStr(const std::string& str = "s3d_camera_proj");
    static void setOrientationStr(const std::string& str = "s3d_camera_orientation");
    static void setPosStr(const std::string& str = "s3d_camera_position");
    static void setRightStr(const std::string& str = "s3d_camera_right");
    static void setUpStr(const std::string& str = "s3d_camera_up");
    static void setForwardStr(const std::string& str = "s3d_camera_forward");
    static const std::string& getViewStr();
    static const std::string& getProjStr();
    static const std::string& getOrientationStr();
    static const std::string& getPosStr();
    static const std::string& getRightStr();
    static const std::string& getUpStr();
    static const std::string& getForwardStr();
private:
    static std::string mViewStr;
    static std::string mProjStr;
    static std::string mOrientationStr;
    static std::string mPosStr;
    static std::string mRightStr;
    static std::string mUpStr;
    static std::string mForwardStr;
};
}
#endif // S3D_CAMERA_H
