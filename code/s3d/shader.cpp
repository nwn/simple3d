#include "shader.h"
s3d::Shader::Shader() {
    setObject(0);
    mVertexObject = 0;
    mFragmentObject = 0;
}
s3d::Shader::~Shader() {
    destroy();
}

void s3d::Shader::destroy() {
    deleteObjects();
}

const bool s3d::Shader::loadFromFile(const std::string& vertexFilepath, const std::string& fragmentFilepath) {
    std::string vsource;
    std::string fsource;

    mVertexFilepath = vertexFilepath;
    mFragmentFilepath = fragmentFilepath;

    try {
        vsource = s3d::util::loadFile(getVertexFilepath());
        fsource = s3d::util::loadFile(getFragmentFilepath());
    } catch(std::exception& e) {
        throw std::runtime_error((std::string)"s3d::Shader: " + e.what());
    }
    return loadFromMemory(vsource, fsource);
}
const bool s3d::Shader::loadFromMemory(const std::string& vertexSource, const std::string& fragmentSource) {
    if(glIsProgram(getObject()) || glIsShader(mVertexObject || glIsShader(mFragmentObject))) {  // this shader has already loaded
        destroy();
        resetVariables();
    }

    mVertexSource   = vertexSource;
    mFragmentSource = fragmentSource;

    GLuint validObj = 0;
    if(!compile(&mVertexObject,   getVertexSource(),   GL_VERTEX_SHADER)   ||
            !compile(&mFragmentObject, getFragmentSource(), GL_FRAGMENT_SHADER) ||
            !link(&validObj)) {
        return false;
    }
    setObject(validObj);
    glDeleteShader(mVertexObject);
    glDeleteShader(mFragmentObject);
    return true;
}
const std::string& s3d::Shader::getVertexSource() const {
    return mVertexSource;
}
const std::string& s3d::Shader::getFragmentSource() const {
    return mFragmentSource;
}
const std::string& s3d::Shader::getVertexFilepath() const {
    return mVertexFilepath;
}
const std::string& s3d::Shader::getFragmentFilepath() const {
    return mFragmentFilepath;
}
const bool s3d::Shader::compile(GLuint *shaderObj, const std::string& code, const GLenum type) {
    s3d_assert(type == GL_VERTEX_SHADER || type == GL_FRAGMENT_SHADER);

    *shaderObj = glCreateShader(type);

    const char* shaderSourceChar = code.c_str();
    glShaderSource(*shaderObj, 1, &shaderSourceChar, nullptr);
    glCompileShader(*shaderObj);
    GLint status;
    glGetShaderiv(*shaderObj, GL_COMPILE_STATUS, &status);
    if(status != GL_TRUE) {
        std::string error;

        GLint logLength;
        glGetShaderiv(*shaderObj, GL_INFO_LOG_LENGTH, &logLength);
        char* infoLog = new char[logLength + 1];
        glGetShaderInfoLog(*shaderObj, logLength, nullptr, infoLog);
        error += infoLog;
        delete[] infoLog;

        mErrorType = type;
        mError     = error;
        throw std::runtime_error(getError());
        return false;
    }
    return true;
}
const bool s3d::Shader::link(GLuint *shaderObj) {
    *shaderObj = glCreateProgram();
    glAttachShader(*shaderObj, mVertexObject);
    glAttachShader(*shaderObj, mFragmentObject);
    glLinkProgram(*shaderObj);

    GLint linkStatus = 0;
    glGetProgramiv(*shaderObj, GL_LINK_STATUS, &linkStatus);
    if(linkStatus == GL_FALSE) {
        std::string error;

        GLint logLength;
        glGetProgramiv(*shaderObj, GL_INFO_LOG_LENGTH, &logLength);
        char* infoLog = new char[logLength + 1];
        glGetProgramInfoLog(*shaderObj, logLength, nullptr, infoLog);
        error += infoLog;
        delete[] infoLog;

        mErrorType = GL_LINK_STATUS;
        mError     = error;
        *shaderObj = 0;
        throw std::runtime_error(getError());
        return false;
    }
    return true;
}
const GLint s3d::Shader::getUniform(const std::string& name) const {
    return glGetUniformLocation(getObject(), name.c_str());
}
const GLint s3d::Shader::getAttribute(const std::string& name) const {
    return glGetAttribLocation(getObject(), name.c_str());
}
const bool s3d::Shader::hasUniform(const std::string& name) const {
    return (getUniform(name) != -1);
}
const bool s3d::Shader::hasAttribute(const std::string& name) const {
    return (getAttribute(name) != -1);
}

#define ATTRIB_AND_UNIFORM_SETTERS(OGL_TYPE, TYPE_PREFIX, TYPE_SUFFIX) \
\
void s3d::Shader::setAttribute(const std::string& name, OGL_TYPE v0) { \
    safeBind(); glVertexAttrib ## TYPE_PREFIX ## 1 ## TYPE_SUFFIX (getSafeAttribute(name), v0); safeUnbind(); \
} \
void s3d::Shader::setAttribute(const std::string& name, OGL_TYPE v0, OGL_TYPE v1) { \
    safeBind(); glVertexAttrib ## TYPE_PREFIX ## 2 ## TYPE_SUFFIX (getSafeAttribute(name), v0, v1); safeUnbind(); \
} \
void s3d::Shader::setAttribute(const std::string& name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2) { \
    safeBind(); glVertexAttrib ## TYPE_PREFIX ## 3 ## TYPE_SUFFIX (getSafeAttribute(name), v0, v1, v2); safeUnbind(); \
} \
void s3d::Shader::setAttribute(const std::string& name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2, OGL_TYPE v3) { \
    safeBind(); glVertexAttrib ## TYPE_PREFIX ## 4 ## TYPE_SUFFIX (getSafeAttribute(name), v0, v1, v2, v3); safeUnbind(); \
} \
 \
void s3d::Shader::setAttribute1v(const std::string& name, const OGL_TYPE* v) { \
    safeBind(); glVertexAttrib ## TYPE_PREFIX ## 1 ## TYPE_SUFFIX ## v (getSafeAttribute(name), v); safeUnbind(); \
} \
void s3d::Shader::setAttribute2v(const std::string& name, const OGL_TYPE* v) { \
    safeBind(); glVertexAttrib ## TYPE_PREFIX ## 2 ## TYPE_SUFFIX ## v (getSafeAttribute(name), v); safeUnbind(); \
} \
void s3d::Shader::setAttribute3v(const std::string& name, const OGL_TYPE* v) { \
    safeBind(); glVertexAttrib ## TYPE_PREFIX ## 3 ## TYPE_SUFFIX ## v (getSafeAttribute(name), v); safeUnbind(); \
} \
void s3d::Shader::setAttribute4v(const std::string& name, const OGL_TYPE* v) { \
    safeBind(); glVertexAttrib ## TYPE_PREFIX ## 4 ## TYPE_SUFFIX ## v (getSafeAttribute(name), v); safeUnbind(); \
} \
 \
void s3d::Shader::setUniform(const std::string& name, OGL_TYPE v0) { \
    safeBind(); glUniform1 ## TYPE_SUFFIX (getSafeUniform(name), v0); safeUnbind(); \
} \
void s3d::Shader::setUniform(const std::string& name, OGL_TYPE v0, OGL_TYPE v1) { \
    safeBind(); glUniform2 ## TYPE_SUFFIX (getSafeUniform(name), v0, v1); safeUnbind(); \
} \
void s3d::Shader::setUniform(const std::string& name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2) { \
    safeBind(); glUniform3 ## TYPE_SUFFIX (getSafeUniform(name), v0, v1, v2); safeUnbind(); \
} \
void s3d::Shader::setUniform(const std::string& name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2, OGL_TYPE v3) { \
    safeBind(); glUniform4 ## TYPE_SUFFIX (getSafeUniform(name), v0, v1, v2, v3); safeUnbind(); \
} \
 \
void s3d::Shader::setUniform1v(const std::string& name, const OGL_TYPE* v, GLsizei count) { \
    safeBind(); safeBind(); glUniform1 ## TYPE_SUFFIX ## v (getSafeUniform(name), count, v); safeUnbind(); \
} \
void s3d::Shader::setUniform2v(const std::string& name, const OGL_TYPE* v, GLsizei count) { \
    safeBind(); glUniform2 ## TYPE_SUFFIX ## v (getSafeUniform(name), count, v); safeUnbind(); \
} \
void s3d::Shader::setUniform3v(const std::string& name, const OGL_TYPE* v, GLsizei count) { \
    safeBind(); glUniform3 ## TYPE_SUFFIX ## v (getSafeUniform(name), count, v); safeUnbind(); \
} \
void s3d::Shader::setUniform4v(const std::string& name, const OGL_TYPE* v, GLsizei count) { \
    safeBind(); glUniform4 ## TYPE_SUFFIX ## v (getSafeUniform(name), count, v); safeUnbind(); \
}

ATTRIB_AND_UNIFORM_SETTERS(GLfloat,  , f);
ATTRIB_AND_UNIFORM_SETTERS(GLdouble, , d);
ATTRIB_AND_UNIFORM_SETTERS(GLint,   I, i);
ATTRIB_AND_UNIFORM_SETTERS(GLuint,  I, ui);

void s3d::Shader::setUniformMatrix2(const std::string& name, const GLfloat* v, GLsizei count, GLboolean transpose) {
    safeBind();
    glUniformMatrix2fv(getUniform(name), count, transpose, v);
    safeUnbind();
}
void s3d::Shader::setUniformMatrix3(const std::string& name, const GLfloat* v, GLsizei count, GLboolean transpose) {
    safeBind();
    glUniformMatrix3fv(getUniform(name), count, transpose, v);
    safeUnbind();
}
void s3d::Shader::setUniformMatrix4(const std::string& name, const GLfloat* v, GLsizei count, GLboolean transpose) {
    safeBind();
    glUniformMatrix4fv(getUniform(name), count, transpose, v);
    safeUnbind();
}
void s3d::Shader::setUniform(const std::string& name, const glm::mat2& m, GLboolean transpose) {
    safeBind();
    glUniformMatrix2fv(getUniform(name), 1, transpose, glm::value_ptr(m));
    safeUnbind();
}
void s3d::Shader::setUniform(const std::string& name, const glm::mat3& m, GLboolean transpose) {
    safeBind();
    glUniformMatrix3fv(getUniform(name), 1, transpose, glm::value_ptr(m));
    safeUnbind();
}
void s3d::Shader::setUniform(const std::string& name, const glm::mat4& m, GLboolean transpose) {
    safeBind();
    glUniformMatrix4fv(getUniform(name), 1, transpose, glm::value_ptr(m));
    safeUnbind();
}
void s3d::Shader::setUniform(const std::string& name, const glm::vec2& v) {
    safeBind();
    setUniform2v(name, glm::value_ptr(v));
    safeUnbind();
}
void s3d::Shader::setUniform(const std::string& name, const glm::vec3& v) {
    safeBind();
    setUniform3v(name, glm::value_ptr(v));
    safeUnbind();
}
void s3d::Shader::setUniform(const std::string& name, const glm::vec4& v) {
    safeBind();
    setUniform4v(name, glm::value_ptr(v));
    safeUnbind();
}

void s3d::Shader::bind() {
    glUseProgram(getObject());
}
const bool s3d::Shader::bound() const {
    GLint currentProgram = 0;
    glGetIntegerv(GL_CURRENT_PROGRAM, &currentProgram);
    return (currentProgram == (GLint)getObject());
}
void s3d::Shader::unbind() {
    glUseProgram(0);
}
GLint s3d::Shader::mCurrentProgram;

const GLint s3d::Shader::getSafeUniform(const std::string& name) const {
    if(hasUniform(name)) {
        return getUniform(name);
    }
    throw std::runtime_error("no uniform named '" + name + "' in shader object " + s3d::util::toString(getObject()));
    return 0;
}
const GLint s3d::Shader::getSafeAttribute(const std::string& name) const {
    if(hasAttribute(name)) {
        return getAttribute(name);
    }
    throw std::runtime_error("no attribute named '" + name + "' in shader object " + s3d::util::toString(getObject()));
    return 0;
}

void s3d::Shader::safeBind() {
    glGetIntegerv(GL_CURRENT_PROGRAM, &mCurrentProgram);
    if(mCurrentProgram != (GLint)getObject())
        bind();
}
void s3d::Shader::safeUnbind() {
    if(mCurrentProgram != (GLint)getObject())
        glUseProgram(mCurrentProgram);
}

const std::string s3d::Shader::getError() const {
    std::string preError = "failed to ";
    if(getErrorType() == GL_LINK_STATUS) {
        preError += "link shader";
    } else {
        preError += "compile ";
        if(getErrorType() == GL_VERTEX_SHADER) {
            if(getVertexFilepath() != "") {
                preError += "'" + getVertexFilepath() + "':\n";
            } else {
                preError += "vertex shader:\n\n" + getVertexSource() + "\n";
            }
        } else if(getErrorType() == GL_FRAGMENT_SHADER) {
            if(getFragmentFilepath() != "") {
                preError += "'" + getFragmentFilepath() + "':\n";
            } else {
                preError += "fragment shader:\n\n" + getFragmentSource() + "\n";
            }
        }
    }
    return "s3d::Shader: " + preError + mError;
}
const GLuint s3d::Shader::getErrorType() const {
    return mErrorType;
}

void s3d::Shader::setObject(const GLuint obj) {
    mObject = obj;
}
const GLuint& s3d::Shader::getObject() const {
    return mObject;
}
void s3d::Shader::deleteObjects() {
    if(mVertexObject   != 0 && glIsShader(mVertexObject))   glDeleteShader(mVertexObject);
    if(mFragmentObject != 0 && glIsShader(mFragmentObject)) glDeleteShader(mFragmentObject);
    if(getObject()     != 0 && glIsProgram(getObject()))    glDeleteProgram(getObject());
}
void s3d::Shader::resetVariables() {
    mVertexSource     = "";
    mFragmentSource   = "";
    mVertexFilepath   = "";
    mFragmentFilepath = "";
    mError            = "";
    mErrorType = 0;
}
