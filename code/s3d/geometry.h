#ifndef S3D_GEOMETRY_H
#define S3D_GEOMETRY_H

#include "shader.h"
#include "texture.h"

#include <glm/glm.hpp>
#include <cstdlib>
#include <vector>
#include <fstream>

namespace s3d {
class Geometry {
public:
    Geometry();
    ~Geometry();

    void destroy();

    void render() const;

    void setVertices(const std::vector<glm::vec3>& vertices);
    void setNormals(const std::vector<glm::vec3>& normals);
    void setUvs(const std::vector<glm::vec2>& uvs);
    const std::vector<glm::vec3>& getVertices() const;
    const std::vector<glm::vec3>& getNormals() const;
    const std::vector<glm::vec2>& getUvs() const;
    const glm::vec3& getSize() const;

    void loadObj(const std::string& path);

    void upload();                                      // load the vertex, normal and uv data on the graphics card
    void attach(s3d::Shader* const shader) const;       // load the vertex, normal and uv data for a shader to use
private:
    void deleteVao();
    void deleteVbo();
    void calculateSize();

    glm::vec3 mSize;
    std::vector<glm::vec3> mVertices;
    std::vector<glm::vec3> mNormals;
    std::vector<glm::vec2> mUvs;
    GLuint mVaoID;
    GLuint mVboVertices;
    GLuint mVboUvs;
    GLuint mVboNormals;

public:
    static void setVertexStr(const std::string& str = "s3d_geometry_vertex");
    static void setNormalStr(const std::string& str = "s3d_geometry_normal");
    static void setUvStr(const std::string& str = "s3d_geometry_uv");
    static const std::string& getVertexStr();
    static const std::string& getNormalStr();
    static const std::string& getUvStr();

    static void Upload();
    static void Destroy();

    static Geometry Cube;
    static Geometry InverseCube;
    static Geometry Quad;
private:
    static std::string mVertexStr;
    static std::string mNormalStr;
    static std::string mUvStr;
};
}

#endif // S3D_GEOMETRY_H
