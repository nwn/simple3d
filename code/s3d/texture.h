#ifndef S3D_TEXTURE_H
#define S3D_TEXTURE_H

#include "util.h"

#include <GL/glew.h>
#include <SOIL.h>

#include <vector>
#include <algorithm>
#include <string>

namespace s3d {
class Texture {
protected:
    Texture();
public:
    enum class Mode {
        Near = GL_TEXTURE_MAG_FILTER,
        Far  = GL_TEXTURE_MIN_FILTER,
        Both = Near | Far
    };
    enum class Wrapping {
        Clamp  = GL_CLAMP_TO_EDGE,
        Repeat = GL_REPEAT
    };
    enum class Filtering {
        Nearest = GL_NEAREST,
        Linear  = GL_LINEAR
    };
    enum class Filter {
        Anisotropic
    };

    virtual void setWrapping(const Wrapping& wrap) = 0;
    virtual void setFiltering(const Filtering& filtering, const Mode& mode = Mode::Both) = 0;
    virtual void addFilter(const Filter& filter) = 0;
    virtual void removeFilter(const Filter& filter) = 0;

    virtual const Wrapping& getWrapping() const = 0;
    virtual const Filtering& getFilteringNear() const = 0;
    virtual const Filtering& getFilteringFar() const = 0;

    static const int getMaximumUnits();

    virtual void destroy() = 0;
    virtual void bind(const int unit = 0) const = 0;
    const GLuint& getObject() const;
    const bool hasObject() const;
    const bool bound() const;
    void setObject(const GLuint object);
private:
    GLuint mObject;
};
class Texture2d : public Texture {
public:
    Texture2d();
    ~Texture2d();

    void loadFromFile(const std::string& path);

    void destroy() override;

    void setWrapping(const Wrapping& wrap) override;
    void setFiltering(const Filtering& filtering, const Mode& mode = Mode::Both) override;
    void addFilter(const Filter& filter) override;
    void removeFilter(const Filter& filter) override;

    const Wrapping& getWrapping() const override;
    const Filtering& getFilteringNear() const override;
    const Filtering& getFilteringFar() const override;

    void bind(const int unit = 0) const override;
    static void unbind();
private:
    Wrapping mWrapping;
    Filtering mFilteringNear;
    Filtering mFilteringFar;
};
class Texture3d : public Texture {
public:
    Texture3d();
    ~Texture3d();

    void loadFromFile(const std::string& path, const std::string& order = "EWNSUD");
    void loadFromFiles(const std::string& xp, const std::string& xn,
                       const std::string& yp, const std::string& yn,
                       const std::string& zp, const std::string& zn);

    void destroy() override;

    void setWrapping(const Wrapping& wrap) override;
    void setFiltering(const Filtering& filtering, const Mode& mode = Mode::Both) override;
    void addFilter(const Filter& filter) override;
    void removeFilter(const Filter& filter) override;

    const Wrapping& getWrapping() const override;
    const Filtering& getFilteringNear() const override;
    const Filtering& getFilteringFar() const override;

    void bind(const int unit = 0) const override;
    static void unbind();
private:
    void setDefaultTextureOptions();

    Wrapping mWrapping;
    Filtering mFilteringNear;
    Filtering mFilteringFar;
};
}

#endif // S3D_TEXTURE_H
