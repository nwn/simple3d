#include "input.h"

s3d::Input::Input() {
    setEnabled(true);
    update();
}

void s3d::Input::setEnabled(const bool enabled) {
    mEnabled = enabled;
}
const bool s3d::Input::isEnabled() const {
    return mEnabled;
}

const bool s3d::Input::keyPressed(const sf::Keyboard::Key key) const {
    if(mGlobalEnabled && mEnabled) {
        return mKeyPressed.at(key);
    }
    return false;
}
const bool s3d::Input::keyWasPressed(const sf::Keyboard::Key key) const {
    if(mGlobalEnabled && mEnabled) {
        return mWasKeyPressed.at(key);
    }
    return false;
}
const bool s3d::Input::keyTapped(const sf::Keyboard::Key key) const {
    return (keyPressed(key) && !keyWasPressed(key));
}

const bool s3d::Input::mousePressed(const sf::Mouse::Button button) const {
    if(mGlobalEnabled && mEnabled) {
        return mButtonPressed.at(button);
    }
    return false;
}
const bool s3d::Input::mouseWasPressed(const sf::Mouse::Button button) const {
    if(mGlobalEnabled && mEnabled) {
        return mWasButtonPressed.at(button);
    }
    return false;
}
const bool s3d::Input::mouseTapped(const sf::Mouse::Button button) const {
    return (mousePressed(button) && !mouseWasPressed(button));
}

void s3d::Input::setGlobalEnabled(const bool enabled) {
    mGlobalEnabled = enabled;
}
const bool s3d::Input::isGlobalEnabled() {
    return mGlobalEnabled;
}

void s3d::Input::update() {
    if(mKeyPressed.size() == 0 || mWasKeyPressed.size() == 0) {
        mKeyPressed.resize(sf::Keyboard::KeyCount);
        mWasKeyPressed.resize(sf::Keyboard::KeyCount);
    }
    if(mButtonPressed.size() == 0 || mWasButtonPressed.size() == 0) {
        mButtonPressed.resize(sf::Mouse::Button::ButtonCount);
        mWasButtonPressed.resize(sf::Mouse::Button::ButtonCount);
    }
    // update keyboard
    for(unsigned int i=0;i<sf::Keyboard::KeyCount;i++) {
        mWasKeyPressed.at(i) = mKeyPressed.at(i);
        mKeyPressed.at(i) = sf::Keyboard::isKeyPressed((sf::Keyboard::Key)(i));
    }
    // update mouse
    for(unsigned int i=0;i<sf::Mouse::Button::ButtonCount;i++) {
        mWasButtonPressed.at(i) = mButtonPressed.at(i);
        mButtonPressed.at(i) = sf::Mouse::isButtonPressed((sf::Mouse::Button)(i));
    }
}

bool s3d::Input::mGlobalEnabled = true;
std::vector<bool> s3d::Input::mKeyPressed;
std::vector<bool> s3d::Input::mWasKeyPressed;
std::vector<bool> s3d::Input::mButtonPressed;
std::vector<bool> s3d::Input::mWasButtonPressed;
