#include "geometry.h"

s3d::Geometry::Geometry() {
    static bool initialized = false;
    if(!initialized) {
        Cube.setVertices({
            glm::vec3(-0.5f, -0.5f, -0.5f),
            glm::vec3(-0.5f,  0.5f, -0.5f),
            glm::vec3( 0.5f, -0.5f, -0.5f),
            glm::vec3( 0.5f,  0.5f, -0.5f),
            glm::vec3( 0.5f, -0.5f, -0.5f),
            glm::vec3(-0.5f,  0.5f, -0.5f),

            glm::vec3(-0.5f, -0.5f, -0.5f),
            glm::vec3(-0.5f, -0.5f,  0.5f),
            glm::vec3(-0.5f,  0.5f, -0.5f),
            glm::vec3(-0.5f,  0.5f,  0.5f),
            glm::vec3(-0.5f,  0.5f, -0.5f),
            glm::vec3(-0.5f, -0.5f,  0.5f),

            glm::vec3( 0.5f, -0.5f,  0.5f),
            glm::vec3( 0.5f, -0.5f, -0.5f),
            glm::vec3( 0.5f,  0.5f,  0.5f),
            glm::vec3( 0.5f,  0.5f, -0.5f),
            glm::vec3( 0.5f,  0.5f,  0.5f),
            glm::vec3( 0.5f, -0.5f, -0.5f),

            glm::vec3(-0.5f,  0.5f,  0.5f),
            glm::vec3(-0.5f, -0.5f,  0.5f),
            glm::vec3( 0.5f,  0.5f,  0.5f),
            glm::vec3( 0.5f, -0.5f,  0.5f),
            glm::vec3( 0.5f,  0.5f,  0.5f),
            glm::vec3(-0.5f, -0.5f,  0.5f),

            glm::vec3( 0.5f,  0.5f, -0.5f),
            glm::vec3(-0.5f,  0.5f, -0.5f),
            glm::vec3( 0.5f,  0.5f,  0.5f),
            glm::vec3(-0.5f,  0.5f,  0.5f),
            glm::vec3( 0.5f,  0.5f,  0.5f),
            glm::vec3(-0.5f,  0.5f, -0.5f),

            glm::vec3(-0.5f, -0.5f,  0.5f),
            glm::vec3(-0.5f, -0.5f, -0.5f),
            glm::vec3( 0.5f, -0.5f, -0.5f),
            glm::vec3(-0.5f, -0.5f,  0.5f),
            glm::vec3( 0.5f, -0.5f, -0.5f),
            glm::vec3( 0.5f, -0.5f,  0.5f)});
        InverseCube.setVertices({
            glm::vec3(-0.5f,  0.5f, -0.5f),
            glm::vec3(-0.5f, -0.5f, -0.5f),
            glm::vec3( 0.5f, -0.5f, -0.5f),
            glm::vec3( 0.5f, -0.5f, -0.5f),
            glm::vec3( 0.5f,  0.5f, -0.5f),
            glm::vec3(-0.5f,  0.5f, -0.5f),

            glm::vec3(-0.5f, -0.5f,  0.5f),
            glm::vec3(-0.5f, -0.5f, -0.5f),
            glm::vec3(-0.5f,  0.5f, -0.5f),
            glm::vec3(-0.5f,  0.5f, -0.5f),
            glm::vec3(-0.5f,  0.5f,  0.5f),
            glm::vec3(-0.5f, -0.5f,  0.5f),

            glm::vec3( 0.5f, -0.5f, -0.5f),
            glm::vec3( 0.5f, -0.5f,  0.5f),
            glm::vec3( 0.5f,  0.5f,  0.5f),
            glm::vec3( 0.5f,  0.5f,  0.5f),
            glm::vec3( 0.5f,  0.5f, -0.5f),
            glm::vec3( 0.5f, -0.5f, -0.5f),

            glm::vec3(-0.5f, -0.5f,  0.5f),
            glm::vec3(-0.5f,  0.5f,  0.5f),
            glm::vec3( 0.5f,  0.5f,  0.5f),
            glm::vec3( 0.5f,  0.5f,  0.5f),
            glm::vec3( 0.5f, -0.5f,  0.5f),
            glm::vec3(-0.5f, -0.5f,  0.5f),

            glm::vec3(-0.5f,  0.5f, -0.5f),
            glm::vec3( 0.5f,  0.5f, -0.5f),
            glm::vec3( 0.5f,  0.5f,  0.5f),
            glm::vec3( 0.5f,  0.5f,  0.5f),
            glm::vec3(-0.5f,  0.5f,  0.5f),
            glm::vec3(-0.5f,  0.5f, -0.5f),

            glm::vec3(-0.5f, -0.5f, -0.5f),
            glm::vec3(-0.5f, -0.5f,  0.5f),
            glm::vec3( 0.5f, -0.5f, -0.5f),
            glm::vec3( 0.5f, -0.5f, -0.5f),
            glm::vec3(-0.5f, -0.5f,  0.5f),
            glm::vec3( 0.5f, -0.5f,  0.5f)});
        Quad.setVertices({
            glm::vec3(-1.0f, -1.0f, 0.f),
            glm::vec3( 1.0f, -1.0f, 0.f),
            glm::vec3(-1.0f,  1.0f, 0.f),

            glm::vec3(-1.0f,  1.0f, 0.f),
            glm::vec3( 1.0f, -1.0f, 0.f),
            glm::vec3( 1.0f, 1.0f, 0.f)});
        Quad.setUvs({
            glm::vec2(0, 0),
            glm::vec2(1, 0),
            glm::vec2(0, 1),

            glm::vec2(0, 1),
            glm::vec2(1, 0),
            glm::vec2(1, 1)});
    }

    mVaoID = 0;
    mVboVertices = 0;
    mVboUvs = 0;
    mVboNormals = 0;
}
s3d::Geometry::~Geometry() {
    destroy();
}

void s3d::Geometry::destroy() {
    deleteVbo();
    deleteVao();
}

void s3d::Geometry::render() const {
    if(mVaoID != 0) {
        glBindVertexArray(mVaoID);
        glDrawArrays(GL_TRIANGLES, 0, mVertices.size());
        glBindVertexArray(0);
    }
}

void s3d::Geometry::setVertices(const std::vector<glm::vec3>& vertices) {
    mVertices = vertices;
    calculateSize();
}
void s3d::Geometry::setNormals(const std::vector<glm::vec3>& normals) {
    mNormals = normals;
}
void s3d::Geometry::setUvs(const std::vector<glm::vec2>& uvs) {
    mUvs = uvs;
}
const std::vector<glm::vec3>& s3d::Geometry::getVertices() const {
    return mVertices;
}
const std::vector<glm::vec3>& s3d::Geometry::getNormals() const {
    return mNormals;
}
const std::vector<glm::vec2>& s3d::Geometry::getUvs() const {
    return mUvs;
}
const glm::vec3& s3d::Geometry::getSize() const {
    return mSize;
}

void s3d::Geometry::loadObj(const std::string& path) {
    try {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> uvs;

        std::vector<int> faceVertices;
        std::vector<int> faceNormals;
        std::vector<int> faceUvs;

        std::ifstream source(path.c_str());
        if(source) {
            for(std::string line; std::getline(source, line); ) {
                std::istringstream stream(line);

                std::string type;
                stream >> type;
                if(type == "v") {
                    float x, y, z;
                    stream >> x >> y >> z;
                    vertices.push_back(glm::vec3(x, y, z));
                } else if(type == "vn") {
                    float x, y, z;
                    stream >> x >> y >> z;
                    normals.push_back(glm::vec3(x, y, z));
                } else if(type == "vt") {
                    float x, y;
                    stream >> x >> y;
                    uvs.push_back(glm::vec2(x, y));
                } else if(type == "f") {
                    std::vector<std::string> chunks;
                    chunks.resize(3);
                    stream >> chunks.at(0) >> chunks.at(1) >> chunks.at(2);
                    for(unsigned int i=0;i<chunks.size();i++) {
                        std::stringstream chunkstream(chunks.at(i));
                        std::string s;
                        int typeCounter = 0;
                        while(std::getline(chunkstream, s, '/')) {
                            if(s.size() > 0) {
                                int sectionData = atoi(s.c_str());
                                if(typeCounter == 0) {
                                    faceVertices.push_back(sectionData);
                                } else if(typeCounter == 1) {
                                    faceUvs.push_back(sectionData);
                                } else if(typeCounter == 2) {
                                    faceNormals.push_back(sectionData);
                                }
                            }
                            typeCounter++;
                        }
                    }
                }
            }
            // convert the read data into the member variables
            std::vector<glm::vec3> memberVertices;
            std::vector<glm::vec3> memberNormals;
            std::vector<glm::vec2> memberUvs;

            for(unsigned int i=0;i<faceVertices.size();i++) {
                memberVertices.push_back(vertices.at(faceVertices.at(i)-1));
            }
            for(unsigned int i=0;i<faceNormals.size();i++) {
                memberNormals.push_back(normals.at(faceNormals.at(i)-1));
            }
            for(unsigned int i=0;i<faceUvs.size();i++) {
                memberUvs.push_back(uvs.at(faceUvs.at(i)-1));
            }

            setVertices(memberVertices);
            setNormals(memberNormals);
            setUvs(memberUvs);
        } else {
            throw std::runtime_error("failed to open file '" + path + "'");
        }
    } catch(std::exception& e) {
        throw std::runtime_error((std::string)"s3d::Geometry::loadObj: " + e.what());
    }
}

void s3d::Geometry::upload() {
    destroy();

    glGenVertexArrays(1, &mVaoID);
    glBindVertexArray(mVaoID);

    // verticies
    if(getVertices().size() > 0) {
        glGenBuffers(1, &mVboVertices);
        glBindBuffer(GL_ARRAY_BUFFER, mVboVertices);
        glBufferData(GL_ARRAY_BUFFER, getVertices().size() * sizeof(glm::vec3), &getVertices().at(0), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    // normals
    if(getNormals().size() > 0) {
        glGenBuffers(1, &mVboNormals);
        glBindBuffer(GL_ARRAY_BUFFER, mVboNormals);
        glBufferData(GL_ARRAY_BUFFER, getNormals().size() * sizeof(glm::vec3), &getNormals().at(0), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    // uvs
    if(getUvs().size() > 0) {
        glGenBuffers(1, &mVboUvs);
        glBindBuffer(GL_ARRAY_BUFFER, mVboUvs);
        glBufferData(GL_ARRAY_BUFFER, getUvs().size() * sizeof(glm::vec2), &getUvs().at(0), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    glBindVertexArray(0);
}

void s3d::Geometry::attach(s3d::Shader* const shader) const {
    const std::string shaderVertex = getVertexStr();
    const std::string shaderNormal = getNormalStr();
    const std::string shaderUv     = getUvStr();

    const bool shaderHasVertex = shader->hasAttribute(shaderVertex);
    const bool shaderHasNormal = shader->hasAttribute(shaderNormal);
    const bool shaderHasUv = shader->hasAttribute(shaderUv);

    glBindVertexArray(mVaoID);
        if(shaderHasVertex && mVboVertices != 0) {
            glBindBuffer(GL_ARRAY_BUFFER, mVboVertices);
            glEnableVertexAttribArray(shader->getAttribute(shaderVertex));
            glVertexAttribPointer(shader->getAttribute(shaderVertex), 3, GL_FLOAT, GL_FALSE, 0, 0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }
        if(shaderHasNormal && mVboNormals != 0) {
            glBindBuffer(GL_ARRAY_BUFFER, mVboNormals);
            glEnableVertexAttribArray(shader->getAttribute(shaderNormal));
            glVertexAttribPointer(shader->getAttribute(shaderNormal), 3, GL_FLOAT, GL_FALSE, 0, 0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }
        if(shaderHasUv && mVboUvs != 0) {
            glBindBuffer(GL_ARRAY_BUFFER, mVboUvs);
            glEnableVertexAttribArray(shader->getAttribute(shaderUv));
            glVertexAttribPointer(shader->getAttribute(shaderUv), 2, GL_FLOAT, GL_FALSE, 0, 0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }
    glBindVertexArray(0);
}

void s3d::Geometry::deleteVao() {
    if(mVaoID != 0) glDeleteVertexArrays(1, &mVaoID);

    mVaoID = 0;
}
void s3d::Geometry::deleteVbo() {
    if(mVboVertices != 0) glDeleteBuffers(1, &mVboVertices);
    if(mVboUvs != 0) glDeleteBuffers(1, &mVboUvs);
    if(mVboNormals != 0) glDeleteBuffers(1, &mVboNormals);

    mVboVertices = 0;
    mVboUvs = 0;
    mVboNormals = 0;
}

void s3d::Geometry::calculateSize() {
    glm::vec3 max = glm::vec3(0, 0, 0);
    glm::vec3 min = glm::vec3(0, 0, 0);
    if(!getVertices().empty()) {
        for(unsigned int i = 1; i < getVertices().size(); ++i) {
            glm::vec3 vertex = getVertices().at(i);

            min.x = std::min(min.x, vertex.x);
            min.y = std::min(min.y, vertex.y);
            min.z = std::min(min.z, vertex.z);

            max.x = std::max(max.x, vertex.x);
            max.y = std::max(max.y, vertex.y);
            max.z = std::max(max.z, vertex.z);
        }
    }
    mSize = max - min;
}

void s3d::Geometry::setVertexStr(const std::string& str) {
    mVertexStr = str;
}
void s3d::Geometry::setNormalStr(const std::string& str) {
    mNormalStr = str;
}
void s3d::Geometry::setUvStr(const std::string& str) {
    mUvStr = str;
}
const std::string& s3d::Geometry::getVertexStr() {
    if(mVertexStr.size() <= 0) {
        setVertexStr();
    }
    return mVertexStr;
}
const std::string& s3d::Geometry::getNormalStr() {
    if(mNormalStr.size() <= 0) {
        setNormalStr();
    }
    return mNormalStr;
}
const std::string& s3d::Geometry::getUvStr() {
    if(mUvStr.size() <= 0) {
        setUvStr();
    }
    return mUvStr;
}

void s3d::Geometry::Upload() {
    Cube.upload();
    InverseCube.upload();
    Quad.upload();
}
void s3d::Geometry::Destroy() {
    Cube.destroy();
    InverseCube.destroy();
    Quad.destroy();
}

s3d::Geometry s3d::Geometry::Cube;
s3d::Geometry s3d::Geometry::InverseCube;
s3d::Geometry s3d::Geometry::Quad;

std::string s3d::Geometry::mVertexStr;
std::string s3d::Geometry::mNormalStr;
std::string s3d::Geometry::mUvStr;
