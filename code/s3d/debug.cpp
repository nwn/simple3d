#include "debug.h"
void s3d::debug::s3d_assert_(const char* const condition, const char* const filename, const int line, const char* const function) {
    static std::vector<std::string> previousAssertions;
    const std::string sCondition = condition;
    const std::string sFilename = filename;
    const std::string sLine = s3d::util::toString(line);
    const std::string sFunction = function;

    const std::string output = "simple3d assertion [ "+sCondition+" ] failed in [ "+sFilename+" ] on line "+sLine+" in [ "+sFunction+" ]\n";
    std::cout << output;

    bool previous = false;
    for(unsigned int i=0;i<previousAssertions.size();i++) {
        if(output == previousAssertions.at(i)) {
            previous = true;
            i = previousAssertions.size();  // break out of loop
        }
    }
    if(!previous) {
        std::string choice;
        do {
            std::cout << "enter 'x' to exit, 'i' to ignore\n";
            std::cin >> choice;
        } while(choice != "x" && choice != "i");

        if(choice == "i") {
            previousAssertions.push_back(output);
        } else {
            throw std::runtime_error("simple3d assertion: exit");
        }
    }
}
const std::string s3d::debug::failedToLoad(const std::string& filepath) {
    return "failed to load '" + filepath + "'";
}
const std::string s3d::debug::noElementExists(const std::string& name) {
    return "no element named '" + name + "'";
}
const std::string s3d::debug::elementExists(const std::string& name) {
    return "element '" + name + "' already exists";
}
