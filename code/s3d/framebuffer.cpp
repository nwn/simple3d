#include "framebuffer.h"

s3d::Framebuffer::Framebuffer() {
    setObject(0);
    setDepthBuffer(0);
    resetType();
}
s3d::Framebuffer::~Framebuffer() {
    destroy();
}

void s3d::Framebuffer::create(const glm::vec2& size, const Type& type) {
    destroy();
    setType(type);

    // create and bind the frame buffer
    GLuint newObject = 0;
    glGenFramebuffers(1, &newObject);
    setObject(newObject);
    bind();

    // create the texture
    GLuint newTexture = 0;
    glGenTextures(1, &newTexture);
    texture()->setObject(newTexture);
    texture()->bind();
    if(type == Type::Color) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
    }
//   else if(type == Type::Depth) {
//        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, size.x, size.y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
//        glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_LUMINANCE);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
//    }
    texture()->setFiltering(s3d::Texture::Filtering::Linear);
    texture()->setWrapping(s3d::Texture::Wrapping::Repeat);
    if(type == Type::Color) {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture()->getObject(), 0);
    }
//    else if(type == Type::Depth) {
//        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texture()->getObject(), 0);
//    }

    // check if framebuffer is valid
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        throw std::runtime_error("failed to create s3d::Framebuffer");
    }
    s3d::Texture2d::unbind();

    // create the depth buffer
    GLuint newRenderbuffer = 0;
    glGenRenderbuffers(1, &newRenderbuffer);
    setDepthBuffer(newRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, getDepthBuffer());
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, size.x, size.y);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, getDepthBuffer());
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    unbind();
}
void s3d::Framebuffer::destroy() {
    // delete opengl objects
    texture()->destroy();

    GLuint renderbuffer = getDepthBuffer();
    GLuint object = getObject();
    if(hasDepthBuffer()) {
        glDeleteRenderbuffers(1, &renderbuffer);
    }
    if(hasObject()) {
        glDeleteFramebuffers(1, &object);
    }
    setDepthBuffer(0);
    setObject(0);
}

const s3d::Framebuffer::Type& s3d::Framebuffer::getType() const {
    return mType;
}

void s3d::Framebuffer::bind() const {
    glBindFramebuffer(GL_FRAMEBUFFER, mObject);
}
void s3d::Framebuffer::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
const bool s3d::Framebuffer::bound() const {
    GLint object;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &object);
    return (object == (GLint)getObject());
}

const GLuint s3d::Framebuffer::getObject() const {
    return mObject;
}
const bool s3d::Framebuffer::hasObject() const {
    return (mObject > 0);
}
void s3d::Framebuffer::setObject(const GLuint object) {
    mObject = object;
}

const s3d::Texture2d* s3d::Framebuffer::texture() const {
    return &mTexture;
}
s3d::Texture2d* s3d::Framebuffer::texture() {
    return &mTexture;
}

void s3d::Framebuffer::resetType() {
    setType(Type::Color);
}
void s3d::Framebuffer::setType(const Type& type) {
    mType = type;
}

const GLuint s3d::Framebuffer::getDepthBuffer() const {
    return mDepthBuffer;
}
const bool s3d::Framebuffer::hasDepthBuffer() const {
    return (mDepthBuffer > 0);
}
void s3d::Framebuffer::setDepthBuffer(const GLuint object) {
    mDepthBuffer = object;
}
