#ifndef S3D_FRAMEBUFFER_H
#define S3D_FRAMEBUFFER_H

#include "texture.h"
#include "window.h"
#include "shader.h"
#include "model.h"

namespace s3d {
class Framebuffer {
public:
    enum class Type {
        Color,
//        Depth
//        Stencil
    };

    Framebuffer();
    ~Framebuffer();

    void create(const glm::vec2& size, const Type& type);
    void destroy();

    const Type& getType() const;

    void bind() const;
    static void unbind();
    const bool bound() const;

    const GLuint getObject() const;
    const bool hasObject() const;
    void setObject(const GLuint object);

    const s3d::Texture2d* texture() const;
    s3d::Texture2d* texture();
private:
    void resetType();
    void setType(const Type& type);

    const GLuint getDepthBuffer() const;
    const bool hasDepthBuffer() const;
    void setDepthBuffer(const GLuint object);

    Type mType;
    GLuint mObject;
    GLuint mDepthBuffer;
    s3d::Texture2d mTexture;
};
}

#endif // S3D_FRAMEBUFFER_H
